# my-linux-configs 
Configurations and scripts that I created or adapted for use in my Linux systems. This repository will make it easier for me to synchronize my setup and to share them with anyone who might be interested.

The directory structure of this repository does not specify the absolute paths of the files. Since the configuration process may very wildly between different distros any caveats are noted in the comments or elsewhere. Shared files between systems will go in the root directory of this repository.

### Systems

**Desktop**
* OS: Mint 17.3 Rosa
* CPU: Intel Core i5-4690K @ 3.5GHz
* Motherboard: MSI Z97 Gaming 5
* Memory: PNY XLR8 8GB DDR3 1866
* Storage: 500GB Maxtor 6H500F0
* Video Card: Sapphire Radeon R9 290 DX12
* Case: Corsair 760T Full-Tower
* Power Supply: EVGA SuperNOVA 850W G2 

**Laptop (Lenovo X200)**
* OS: Peppermint 7
* CPU: Intel Core2 Duo CPU P8600 @ 2.4GHz
* Motherboard: N/A
* Memory: 4GB
* Storage: 144GB HDD
* Graphics: Intel GM45 Express Chipset

